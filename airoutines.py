#!/usr/bin/python
from __future__ import division
from __future__ import print_function

"""Artificial Intelligence routines"""

import pygame

from defines import *

import permutations

class AI(object):
    def __init__(self, initial_game, player):
        self.initial_game = initial_game
        self.player = player


    def think(self, iterations=None):
        """For a specified game and player, try out a number of different moves
        and return the best one.

        Returns a tuple (unit, cell).
        """

        # prepare our hypothetical state
        it_count = 0

        best_move = (player.unit[0], initial_game.gamemap.get_at(0, 0))
        best_score = 0

    def get_new_game(self):
        """get a new game object, as it was originally passed to us"""
        game = copy.copy(self.initial_game)
#        initial_game = copy.copy(initial_game)
        initial_game.he



# main
if __name__ == "__main__":
    import game
    game.main()
