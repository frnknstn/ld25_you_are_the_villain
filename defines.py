#!/usr/bin/python
from __future__ import division
from __future__ import print_function

from pygame.rect import Rect

DEBUG = False
DEBUG = True

RESOURCE_CATEGORIES = ("tiles", "interface", "icons", "music", "sound")

CAP_FPS = 90

DOUBLE_SIZE = False
#DOUBLE_SIZE = True

TILE_WIDTH = 55
TILE_HEIGHT = 64
TILE_SPACING = 49

# distance between the origin of a cell tile and an icon
ICON_OFFSET_WIDTH = 0
ICON_OFFSET_HEIGHT = 4

DIRECTIONS = ("none", "ne", "e", "se", "sw", "w", "nw")

GAME_STATES = set(("pre-game", "setup", "main", "post-game"))

# layout of the map.
# Note that because we use a 'fat' hex grid, the number of cells per row varies:
# For even-numbered rows it is 8 cells, for odd rows only 7.
# therefore the number of cells is NOT rows * columns.
MAP_CELL_ROWS = 8
MAP_CELL_COLUMNS = 8
MAP_CELLS = 60
MAP_CELL_TYPES = 3

# interface layout

#SCREEN_SIZE = (406, 393)
SCREEN_SIZE = (640, 400)

MAP_RECT = Rect((0, 0), (433, 400))

INFO_RECT = Rect((440, 0), (200, 440))
SCORES_RECT = Rect((440, 155), (200, 245))
PLAYER_RECTS = (
    Rect((445, 155), (195, 59)),
    Rect((445, 215), (195, 59)),
    Rect((445, 275), (195, 59)),
    Rect((445, 335), (195, 59))
    )

# offsets from the start of the interface rect
PLAYER_INTERFACE_OFFSETS = {
    "line": (0, 0),
    "title": (2, 4),
    "icon_frame": (2, 21),
    "icon": (4, 23),
    "status": (39, 32)
    }

DEFAULT_FONT = "DejaVu Sans"


# functions

def debug(*args):
    if DEBUG:
        print(*args)


if __name__ == "__main__":
    import game
    game.main()
