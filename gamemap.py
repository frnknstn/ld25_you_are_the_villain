#!/usr/bin/python
from __future__ import division
from __future__ import print_function

"""game map objects"""

import random
import math

import pygame

from defines import *
from resource import res

import hexgrid
import permutations

class GameMap(object):
    def __init__(self):
        self.hexgrid = hexgrid.HexGridFat(MAP_CELL_ROWS, MAP_CELL_COLUMNS, res("tiles", "galaxy_1"))

        self.game_id = None

        self.tile_highlight = res("interface", "tile_highlight")

        self.generate_cell_rects()

    def draw(self, surf):
        self.draw_hexgrid(surf)

    def draw_hexgrid(self, surf):
        """Draw all the sprites in a hexgrid to a surface"""
        hg = self.hexgrid

        debug("drawing hexgrid")
        #debug("drawing hexgrid %s" % str(hg))
        
        surf.fill((0,0,0), MAP_RECT)

        even = True
        y = 0

        for row in hg.data:
            if even:
                x = 0
            else:
                x = TILE_WIDTH // 2

            for cell in row:
                if cell.active:
                    surf.blit(cell.surf, (x, y))
                    if cell.highlighted:
                        surf.blit(self.tile_highlight, (x, y), special_flags=pygame.BLEND_ADD)
                x += TILE_WIDTH - 1

            y += TILE_SPACING - 1
            even = not even

    def generate_cell_rects(self):
        """add the rect-like attributes to the cells"""
        hg = self.hexgrid
        even = True
        y = 0
        cell_x = cell_y = 0

        for row in hg.data:
            if even:
                x = 0
            else:
                x = TILE_WIDTH // 2

            for cell in row:
                cell.rect = pygame.Rect((x, y), (TILE_WIDTH, TILE_HEIGHT))
                cell.coords = (cell_x, cell_y)
                x += TILE_WIDTH - 1
                cell_x += 1

            y += TILE_SPACING - 1
            even = not even
            cell_y += 1
            cell_x = 0

    def random_map(self):
        """generate a random map"""

        pieces = [0] * 30 + [1] * 20 + [2] * 10
        random.shuffle(pieces)

        self.hexgrid.from_seq(pieces)
        self.update_map_details()

        self.game_id = permutations.game_to_id(self.hexgrid.to_seq())
        print("generated new map with game ID %d" % self.game_id)

    def from_game_id(self, game_id):
        self.hexgrid.from_seq(permutations.id_to_game(game_id))
        self.update_map_details()

        self.game_id = game_id
        print("populated map with game ID %d" % self.game_id)

    def update_map_details(self):
        """something may have changed a cell's details, recalculate."""
        for cell in self.hexgrid:
            if cell.data == 0:
                cell.value = 1
                cell.surf = res("tiles", "galaxy_1")
            elif cell.data == 1:
                cell.value = 2
                cell.surf = res("tiles", "galaxy_2")
            elif cell.data == 2:
                cell.value = 3
                cell.surf = res("tiles", "galaxy_3")
            cell.highlighted = False

    def click_cell(self, pos):
        """work out which cell best corresponds to the given X/Y coordinates

        Returns a cell object, or None.
        """
        cells = tuple(self.hexgrid)
        click = pygame.Rect((pos), (1, 1))

        debug(click, cells[0].rect)

        collisions = click.collidelistall(cells)

        if len(collisions) == 0:
            return None
        elif len(collisions) == 1:
            debug("mouse click on cell %d" % collisions[0])
            return cells[collisions[0]]

        # multiple hexes, return the nearest one
        distances = [ rect_distance(click, cells[x].rect) for x in collisions ]
        debug("cells", collisions, distances)

        min_distance = min(distances)
        debug("nearest cell is %0.3f pixels away" % min_distance)

        # too close to call
        if len([ x for x in distances if x == min_distance ]) > 1:
            debug("user clicked equidistant cells")
            return None

        # also too close
        for this_distance in distances:
            if min_distance != this_distance and this_distance - min_distance < 2:
                debug("user click is indeterminate")
                return None

        # we have a winner:
        for index, distance in zip(collisions, distances):
            if distance == min_distance:
                debug("mouse click on cell %d" % index)
                return cells[index]

    def clear_highlights(self):
        """disable the highlighting on all our cells"""
        for cell in self.hexgrid:
            cell.highlighted = False

    def get_line(self, cell, direction):
        """returns a list of all cells along a line in direction, starting from
        a specified cell.
        """

        assert direction in DIRECTIONS

        if direction == "none":
            return [cell]        

        start_cell = cell
        x, y = cell.coords
        retval = []
        
        try:
            # loop through the cells in a direction, until we go off the map
            while cell.active and x >= 0 and y >= 0:
                retval.append(cell)
                x, y = self.hexgrid.adjacent_coords((x, y), direction)
                cell = self.hexgrid.get_at(x, y)
        except IndexError:
            # we went off the map
            debug("off the edge of the map")
            pass

        debug("line from %s to the %s: %s" % (str(start_cell.coords), direction, str([x.coords for x in retval])))

        return retval

    def get_adjacent_cells(self, cell):
        """returns all the cells that are adjacent to the supplied cell, and active."""
        retval = []

        hg = self.hexgrid
        coords = cell.coords

        for direction in ( "ne", "e", "se", "sw", "w", "nw"):
            x, y = hg.adjacent_coords(cell.coords, direction)
            try:
                adj_cell = hg.get_at(x, y)
            except IndexError:
                pass
            else:
                if adj_cell.active:
                    retval.append(adj_cell)

        return retval




        
def rect_distance(a, b):
    """calculates the distance between the center of two rects"""
    x1, y1 = a.center
    x2, y2 = b.center
    return math.sqrt(abs(x1 - x2)**2 + abs(y1 - y2)**2) 









if __name__ == "__main__":
    import game
    game.main()
