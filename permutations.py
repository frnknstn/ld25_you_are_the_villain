#!/usr/bin/python
from __future__ import division

from defines import *

# game ID ideas: if there is no way to extend Lehmer codes to work on unique
# permutations, it may be okay to just work from the 3^60 idea and deal with 
# the four extra decimal digits it introduces...


def id_to_game(game_id):
    """Convert a game ID to the corresponding list of index values."""
    assert game_id <= MAP_CELL_TYPES ** MAP_CELLS
    assert game_id > 0

    game = [0] * MAP_CELLS

    for i in range(MAP_CELLS):
        quotient, remainder = divmod(game_id, MAP_CELL_TYPES)
        game[i] = int(remainder)
        game_id = quotient

    return game

def game_to_id(game):
    """convert a sequence to a integer representing the game."""
    assert len(game) > 0

    # parse the game
    game_id = 0
    index = 0

    for cell in game:
        game_id += cell * (MAP_CELL_TYPES ** index)
        index += 1

    assert game_id <= MAP_CELL_TYPES ** MAP_CELLS

    return game_id


def test_id(game_id):
    """Turn a game ID to a map and back, see if it works out the same"""
    print("Testing game ID %d" % game_id)
    game = id_to_game(game_id)
    print("game: %s" % str(game))
    new_game_id = game_to_id(game)
    print("new game ID is %d" % new_game_id)

    if new_game_id == game_id:
        print("OK")
        return True
    else:
        print("FAIL")
        return False


if __name__ == "__main__":
    import game
    game.main()
