#!/usr/bin/python
from __future__ import division
from __future__ import print_function

import os

import pygame

from defines import *
from resource import res


class InterfaceItem(object):
    def __init__(self, surf, rect):
        self.surf = surf
        self.rect = rect
        interface_list.append(self)
        self.dirty = False

    def draw(self):
        pygame.draw.rect(self.surf, (0,0,0), self.rect, 0)
        self.dirty = False

# class MapBox(InterfaceItem):
#     """The main map display"""
#     def __init__(self, surf, rect):
#         debug("Making a new MapBox item!")
#         super(MapBox, self).__init__(surf, rect)

#     def draw(self):
#         surf.blit(self.surf, self.rect)

class PlayerBox(InterfaceItem):
    """Interface that displays information about a particular player"""
    def __init__(self, surf, rect):
        debug("Making a new PlayerBox item!")
        super(PlayerBox, self).__init__(surf, rect)

        self.title_font = pygame.font.SysFont("DejaVu Sans", 14)
        self.status_font = pygame.font.SysFont("DejaVu Sans", 12)

        self._title = ""
        self.title = None

        self._status = ""
        self.status = None

        self.icon = None

        self.line_rect = rect.move(PLAYER_INTERFACE_OFFSETS["line"])
        self.line_rect.size = (rect.width, 0)

        self.title_rect = rect.move(PLAYER_INTERFACE_OFFSETS["title"])

        self.icon_frame_rect = rect.move(PLAYER_INTERFACE_OFFSETS["icon_frame"])
        self.icon_frame_surf = res("interface", "icon_frame")

        self.icon_rect = rect.move(PLAYER_INTERFACE_OFFSETS["icon"])
        self.icon_surf = None

        self.status_rect = rect.move(PLAYER_INTERFACE_OFFSETS["status"])

        self.highlighted = False


    def draw(self):
        debug("drawing player box %s" % self.title)
        super(PlayerBox, self).draw()
        surf = self.surf
        pygame.draw.line(surf, (255,255,255), self.line_rect.topleft, self.line_rect.bottomright)
        surf.blit(self.title_surf, self.title_rect)
        surf.blit(self.status_surf, self.status_rect)
        surf.blit(self.icon_frame_surf, self.icon_frame_rect)

        if self.icon_surf is not None:
            surf.blit(self.icon_surf, self.icon_rect)

        # highlight active player
        if self.highlighted:
            debug("drawing player box %s %s highlighted" % (self.title, self))
            hs = pygame.Surface(self.rect.size)
            hs.fill((100,100,100))
            surf.blit(hs, self.rect, special_flags=pygame.BLEND_ADD)

    def set_icon(self, icon):
        self.icon_surf = iconify(icon, 32, 32)

    def highlight(self):
        self.highlighted = True
        self.dirty = True

    def unhighlight(self):
        self.highlighted = False
        self.dirty = True

    # getter and setters for the text fields
    @property
    def title(self):
        return self._title
    @title.setter
    def title(self, value):
        debug("setting title to '%s'" % value)
        self._title = value
        self.title_surf = self.title_font.render(value, True, (255,255,255), (0,0,0))
        self.dirty = True

    @property
    def status(self):
        return self._status
    @status.setter
    def status(self, value):
        debug("setting status to '%s'" % value)
        self._status = value
        self.status_surf = self.status_font.render(value, True, (255,255,255), (0,0,0))
        self.dirty = True



# helper functions
def draw_text(text, surface, rect, font_filename=None, size=10, color=(255,255,255)):
    """Draw a line or paragraph of text, wrapping at a supplied pixel width"""

    if text == "" or text is None:
        return

    # prepare the font
    if font_filename is None:
        font_filename = DEFAULT_FONT

    font = pygame.font.SysFont(font_filename, size)

    # prepare the text
    rendered_lines = []
    lines = text.split("\n")
    width = rect.width

    debug("Wrapping text to %d pixels" % width)

    for line in lines:
        words = line.split(" ")
        line_words = []
        line_text = ""
        
        debug("Parsing line: '%s'" % line)
                
        # keep adding words until you hit the line length
        for index in range(len(words)):
            word = words[index]

            debug("Adding word '%s' to line" % word)
            
            line_words.append(word)
            line_text = " ".join(line_words)

            line_size = font.size(line_text)[0]
            debug("Line is now %d / %d pixels" % (line_size, width))

            if line_size > width:
                # too many words!
                line_text = " ".join(line_words[:-1])
                rendered_lines.append(font.render(line_text, True, color))
                line_words = [word]
                debug("Next line now has word '%s'" % word)
                                     
        # make sure all words in this line have been rendered:
        if len(line_words) != 0:
            line_text = " ".join(line_words)
            rendered_lines.append(font.render(line_text, True, color))

    # draw the paragraph!
    x, y = rect.topleft
    for line_surface in rendered_lines:
        surface.blit(line_surface, (x, y))
        y += font.get_height()


def iconify(surf, width=32, height=32):
    """Return a new surface with the image resized down suitable for an icon.

    The image is scaled without distorting its proportions.
    """

    x, y = surf.get_size()
    src_ratio = x / y
    dest_ratio = width / height

    # scale
    if src_ratio == dest_ratio:
        return pygame.transform.smoothscale(surf, (width, height))
    else:
        if src_ratio > dest_ratio:
            target_size = (width, int(width / src_ratio))
        else:
            target_size =  (int(height * src_ratio), height)
        new_surf = pygame.transform.smoothscale(surf, target_size)

        # paste onto a surface of the desired size
        retval = pygame.Surface((width, height))
        rect = new_surf.get_rect(center=retval.get_rect().center)

        retval.blit(new_surf, rect)
        return retval



# main
interface_list = []

if __name__ == "__main__":
    import game
    game.main()
