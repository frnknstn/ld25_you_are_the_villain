#!/usr/bin/python

"""Resource loading and management"""

from __future__ import division
from __future__ import print_function

import os

import pygame

from defines import *

class ResourceManager(object):
    """Module-level singleton for universal access to media files"""
    def __init__(self):
        self.data = {}

    def load_resources(self):
        for category in RESOURCE_CATEGORIES:
            self.data[category] = {}

            if not os.path.isdir(category):
                print("warning: resource path '%s' does not exist." % category)
                continue

            for filename in os.listdir(category):
                filepath = os.path.join(category, filename)
                name, ext = os.path.splitext(filename)

                if name in self.data[category]:
                    print("warning: duplicate file in category '%s' with name '%s': using second file '%s'." % 
                        (
                            category,
                            name,
                            filepath
                        )
                    )

                # import the data
                debug("loading resource '%s'" % filepath)
                obj = None
                if ext in (".png", ".bmp", ".jpg"):
                    obj = pygame.image.load(filepath)
                elif ext in (".ogg", ".wav"):
                    obj = None
                    # Sound loader not yep implemented

                if obj is not None:
                    self.data[category][name] = obj
                else:
                    print("Error loading resource '%s': no parser for extension %s" % (filepath, ext))


    def res(self, category, name):
        """get a single resource from a particular category"""
        return self.data[category][name]

    def get_all(self, category):
        return self.data[category].values()


# module bound function
_inst = ResourceManager()
load_resources = _inst.load_resources
res = _inst.res
get_all = _inst.get_all

if __name__ == "__main__":
    import game
    game.main()
