#!/usr/bin/python
from __future__ import division
from __future__ import print_function

import time
import random

import pygame
from pygame.rect import Rect

from defines import *
import interface
import resource
from resource import res
import gamemap
import airoutines


class Player(object):
    def __init__(self, title, status, icon, player_box=None, ai=False, local=True):
        # set our player box first so it will get populated
        self.player_box = player_box

        self.title = title
        self.status = status
        self.icon = icon

        self.ai = ai
        self.local = local
        self.score = 0
        self.units = set()

    def set_title(self, val):
        self._title = val
        if self.player_box is not None:
            self.player_box.title = val

    def set_status(self, val):
        self._status = val
        if self.player_box is not None:
            self.player_box.status = val

    def set_icon(self, val):
        self._icon = val
        if self.player_box is not None:
            self.player_box.set_icon(val)

    title = property(lambda self: self._title, set_title)
    status = property(lambda self: self._status, set_status)
    icon = property(lambda self: self._icon, set_icon)

    def __str__(self):
        return "<Player object '%s'>" % self.title


class Unit(object):
    def __init__(self, cell, player):
        debug("adding a new unit for player %s" % str(player))
        self.cell = cell
        self.player = player
        player.units.add(self)

    def draw(self, surf):
        surf.blit(self.player.icon, self.cell.rect.move(ICON_OFFSET_WIDTH, ICON_OFFSET_HEIGHT))

    def die(self):
        debug("I, noble %s, am dead!" % str(self))
        self.player.units.remove(self)



class Game(object):
    def __init__(self):
        self.game_state = "pre-game"

        self.players = []
        self.active_player = None

        self.units = []
        self.selected_unit = None

        self.redraw_map = False

        self.gamemap = None


    def start(self):

        gm = gamemap.GameMap()
        self.gamemap = gm

        gm.random_map()
        gm.draw(screen)

        # set up the players
        # TODO: make class

        player_titles = ("xxxBlink182fanxxx", "Evil Bunny Lord", "Middle-manager of Doom", "Noble Heroes!")
        player_statuses = ("Status: Waiting", "Status: Waiting", "Status: Waiting", "Status: Waiting")

        # draw the interface
        pygame.draw.line(screen, (255,255,255), (SCORES_RECT.left, 0), (SCORES_RECT.left, SCREEN_SIZE[1]))
        player_boxes = [interface.PlayerBox(screen, Rect(x)) for x in PLAYER_RECTS]
        
        player_icons = resource.get_all("icons")
        random.shuffle(player_icons)
        for pb, title, status, icon, is_ai in zip(player_boxes, player_titles, player_statuses, player_icons[:4], (False, True, True, True)):
            player = Player(title, status, icon, pb, ai=is_ai)
            pb.draw()
            self.players.append(player)

        self.change_game_state("setup")
        self.active_player = self.players[0]
        self.active_player.player_box.highlight()
        self.active_player.status = "Status: Placing Unit..."

        flip()

        # main loop
        running = True

        
        print("entering main loop")

        while running:

            # event loop
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key in (pygame.K_ESCAPE, pygame.K_q):
                        running = False

                elif event.type == pygame.MOUSEBUTTONDOWN:
                    debug(event)

                    if not DOUBLE_SIZE:
                        pos = event.pos
                    else:
                        pos = (event.pos[0] // 2, event.pos[1] // 2)

                    # where on the screen did the player click?
                    if pygame.Rect(MAP_RECT).collidepoint(pos):
                        cell = gm.click_cell(pos)
                        if cell is not None:
                            self.handle_cell_click(cell)



                elif event.type == pygame.QUIT:
                    running = False

            # update the game state base on recent events
            self.check_game_state();

            # do updates if needed:
            dirty_screen = False
            for item in interface.interface_list:
                if item.dirty:
                    item.draw()
                    dirty_screen = True

            if self.redraw_map:
                gm.draw(screen)
                for unit in self.units:
                    unit.draw(screen)
                self.redraw_map = False                    
                dirty_screen = True

            if dirty_screen:
                debug("flipping screen buffers due to dirty interface")
                flip()

            time.sleep(0.03)

        self.print_scores()
        debug("Game.main() ending")


    def check_game_state(self):
        """Check the current game situation, and advance to the endgame if need be"""
        assert self.game_state in GAME_STATES

        if self.game_state == "main":
            # if the active player has no units, advance to the next player
            game_over = False
            last_player = self.active_player

            while len(self.active_player.units) == 0:
                debug("Skipping player %s who has no units" % str(self.active_player))
                self.next_player()
                if self.active_player == last_player:
                    game_over = True
                    break

            if game_over:
                print("The game has now ended!")
                self.print_scores()
                self.game_state = "post-game"



    def handle_cell_click(self, cell):
        """The user has clicked on a cell"""

        assert self.game_state in GAME_STATES

        if self.game_state == "setup":
            # add a new unit for the current player
            if cell.active:
                if cell in ( x.cell for x in self.units ):
                    debug("there is already a unit on this cell!")
                    return

                unit = Unit(cell, self.active_player)
                self.units.append(unit)
                unit.draw(screen)

                self.active_player.status = "Status: Waiting"
                self.next_player()
                self.active_player.status = "Status: Placing Unit..."
                
                # are we done with setup?
                needed_units = 2 * 4
                player_units = [ len(x.units) for x in self.players ]
                if sum(player_units) >= needed_units:
                    self.change_game_state("main")
                    self.active_player.status = "Status: Moving Unit..."

        elif self.game_state == "main":
            # we need to move units around

            # does this cell have a unit?
            unit = self.get_cell_unit(cell)

            if self.selected_unit is None:
                # we have not yet selected a unit
                if unit is None:
                    return

                # did we click one of our units?
                if unit.player != self.active_player:
                    return

                # select the unit
                self.select_unit(unit)
                return

            else:
                # We have already selected a unit

                if unit is None:
                    # clicked on a cell with no units

                    # inactive cells
                    if not cell.active:
                        self.unselect_units()
                        return

                    # move the unit if it can reach the cell
                    if cell not in self.valid_moves:
                        debug("not a valid move")
                        self.unselect_units()
                        return
                    else:
                        # move, score, and end the turn!
                        self.move_unit(self.selected_unit, cell)
                        return
                    
                else:
                    # we clicked a different unit

                    # someone else's unit?
                    if unit.player != self.active_player:
                        self.unselect_units()
                        return

                    # our unit, select it
                    self.select_unit(unit)
                    return


    def handle_playerbox_click(self, playerbox):
        """The user has clicked on a playerbox"""
        pass

    def next_player(self):
        self.active_player.player_box.unhighlight()

        activate_next = False
        for player in self.players:
            if activate_next:
                self.active_player = player
                activate_next = False
                break

            if player == self.active_player:
                activate_next = True

        if activate_next:
            self.active_player = self.players[0]

        debug("active player is now %s" % self.active_player)
        self.active_player.player_box.highlight()

    def change_game_state(self, state):
        assert state in GAME_STATES

        self.game_state = state

    def select_unit(self, unit):
        # clear the old selections
        self.gamemap.clear_highlights()

        # make the new selections
        self.selected_unit = unit

        # highlight all the valid moves
        passable = []
        for direction in DIRECTIONS:
            passable.extend(self.get_passable_line(unit.cell, direction))

        for cell in passable:
            cell.highlighted = True

        self.valid_moves = set(passable)

        self.redraw_map = True

    def unselect_units(self):
        """Unselect whatever units we have selected"""
        if self.selected_unit is None:
            return

        self.gamemap.clear_highlights()
        self.selected_unit = None
        self.valid_moves = None
        self.redraw_map = True

    def get_passable_line(self, cell, direction):
        """Get all the cells along a line that are reachable from a supplied cell,
        without crossing over a unit.
        """
        line = self.gamemap.get_line(cell, direction)

        for index, cell in enumerate(line[1:]):
            unit = self.get_cell_unit(cell)

            if unit is not None:
                return line[:index+1]

        return line

    def get_cell_unit(self, cell):
        """Returns the unit found in a cell, or None if there is no unit"""
        unit = None
        for compare_unit in self.units:
            if compare_unit.cell == cell:
                unit = compare_unit
                break
        return unit

    def move_unit(self, unit, cell):
        """Move a unit to a cell, scoring the old cell"""
        debug("Moving %s to %s" % (unit, cell.coords))
        old_cell = unit.cell
        unit.cell = cell

        self.score_cell(unit.player, old_cell)

        # If we managed to isolate any unit, that unit dies and scores the cell it was on
        dead_units = []
        for isolated_unit in self.units:
            isolated_cell = isolated_unit.cell
            isolated = True
    
            for adj_cell in self.gamemap.get_adjacent_cells(isolated_cell):
                if self.get_cell_unit(adj_cell) is None:
                    isolated = False
                    break

            if isolated:
                # this unit is definately isolated
                print("Unit %s is isolated!" % str(isolated_unit))
                dead_units.append(isolated_unit)
                self.score_cell(isolated_unit.player, isolated_cell)
        
        for dead_unit in dead_units:
            dead_unit.die()
            self.units.remove(dead_unit)

        # move to next player

        self.active_player.status = "Status: Waiting"
        self.next_player()
        self.active_player.status = "Status: Placing Unit..."

        self.unselect_units()
        self.redraw_map = True

    def score_cell(self, player, cell):
        """Award a player the points for a cell"""
        player.score += cell.value
        print("%s now has a score of %d" % (player.title, player.score))
        cell.active = False


    def print_scores(self):
        """debug function that prints the player scores"""
        print("Scores:")
        for player in self.players:
            print("%s: %d" % (player.title, player.score))

        lines = ["Scores:"]
        lines.extend(("%s: %d" % (player.title, player.score) for player in self.players))
        interface.draw_text("\n".join(lines), screen, INFO_RECT)
        self.redraw_map = True







def flip():
    if DOUBLE_SIZE:
        double_res = [ x * 2 for x in SCREEN_SIZE ]
        pygame.transform.scale(screen, double_res, real_screen)
    pygame.display.flip()


def main():
    global screen
    global real_screen

    pygame.init()

    if not DOUBLE_SIZE:
        screen = pygame.display.set_mode(SCREEN_SIZE)
    else:
        # set up doublesize if needed
        double_res = [ x * 2 for x in SCREEN_SIZE ]
        real_screen = pygame.display.set_mode(double_res)
        screen = pygame.Surface(SCREEN_SIZE)

    resource.load_resources()

    game = Game()
    game.start()

    # clean up
    pygame.quit()



if __name__ == "__main__":
    main()
    print("Done.")
